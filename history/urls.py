from django.urls import path
from history import views

app_name = 'history'


urlpatterns = [
    # path('', views.loginPage,name="loginPage"),
    path('ClimatoJournaliere', views.ClimatoJournaliereView.as_view(),name="ClimatoJournaliere"),
    path('HistoricValues', views.HistoricValuesView.as_view(), name="HistoricValues"),
    path('Stations', views.StationsView.as_view(), name="Stations"),
    path('HistoricEvents', views.HistoricEventsView.as_view(), name="HistoricEvents"),
    path('HistoricNormales', views.HistoricNormalesView.as_view(), name="HistoricNormales"),
    path('getInfoByDepats', views.getInfoByDepats, name="getInfoByDepats"),
    path('HistoricEventsByHits', views.HistoricEventsByHitsView.as_view(), name="HistoricEventsByHits"),
]
